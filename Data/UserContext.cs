using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Musica.Asp.Data
{
    public class UserContext : IdentityDbContext
    {
        public UserContext()
        {
        }

        public UserContext(DbContextOptions options)
            : base(options)
        {
        }
    }

}