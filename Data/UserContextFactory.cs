using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Musica.Asp.Data
{
    public class UserContextFactory : IDesignTimeDbContextFactory<UserContext>
    {
        public UserContext CreateDbContext(string[] args)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true);
            IConfigurationRoot root = builder.Build();

            DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder();

            var connectionString = root.GetConnectionString("UserMySQL") ?? string.Empty;
            optionsBuilder.UseMySQL(connectionString);

            UserContext context = new UserContext(optionsBuilder.Options);

            return context;
        }
    }
}