# MUSICA : 
Une application en architecture microservice pour une bibliotheque de musiques
***
## MUSICA ASP 
- L'authentification de l'application ce fait avec ASP.NET CORE 
- La base de donné n'étant trop grande alors on va utiliser MySQL 
- L'architecture de l'API sera simple et non hexagonal car ce n'est pas une grosse application 
## TEST 
- TDD : Test Driven Developpement
    - Le dossier qui est : TDD
- Test d'integration :
    - Ajouter une base de donné en mémoire
    - Utiliser un client HTTP
    - [DOCUMENTATION](https://learn.microsoft.com/fr-fr/aspnet/core/test/integration-tests?view=aspnetcore-7.0)
## API Gateway 
- Cette API sera aussi un API Gateway avec [Ocelot API Gateway](https://ocelot.readthedocs.io/en/latest/introduction/gettingstarted.html)
- Ocelot est configuré pour etre utiliser seulement avec <b>.NET 6.0</b>
- <b>api/musica</b>
## Deploiment 
- Docker : 
    - Dockerfile
- Kubernetes : 
    - deploiement.yml
    - service.yml
    - service-nodePort.yml
## Les autres projet necessaire 
- [Musica Nest.js](https://gitlab.com/mahatokyhasina25/musica.nest)
- [Musica Spring Boot](https://gitlab.com/mahatokyhasina25/musica.spring)
- [Musica Laravel Lumen](https://gitlab.com/mahatokyhasina25/musica.lumen)
- [Fichier de configuration de Kubernetes](https://gitlab.com/mahatokyhasina25/musica.k8s.cfg)
## URI  
<table>
    <thead>
        <tr>
            <th>Description</th>
            <th>TECHNO</th>
            <th>METHODE</th>
            <th>URI</th>
            <th>BODY</th>
        </tr>
    <thead>
    <tbody>
        <tr>
            <td>Pour creer un compte</td>
            <td>ASP.NET CORE</td>
            <td>POST</td>
            <td>api/musica/registre</td>
            <td>
            {
            "email": "Wiz.khalifa@bet261.com",
            "password": "#Bet2120."
            }
            </td>
        </tr>
        <tr>
            <td>Pour se connecter</td>
            <td>ASP.NET CORE</td>
            <td>POST</td>
            <td>api/musica/login</td>
            <td>
            {
            "email": "Wiz.khalifa@bet261.com",
            "password": "#Bet2120."
            }
            </td>
        </tr>
        <tr>
            <td>Pour creer une music</td>
            <td>Nest.js</td>
            <td>POST</td>
            <td>api/musica/music</td>
            <td>
            {
                "title":"Paris",
                "dateOut":"2018-01-01",
                "singer":[
                    "Nsika"
                ],
                "albumId":"f3a33874-b47e-436a-8b6b-49d66ab8c381"
            }
            </td>
        </tr>
        <tr>
            <td>Pour prendre la liste des musics</td>
            <td>Nest.js</td>
            <td>GET</td>
            <td>api/musica/music</td>
            <td> 
            </td>
        </tr>
        <tr>
            <td>Pour prendre une seule music</td>
            <td>Nest.js</td>
            <td>GET</td>
            <td>api/musica/music/{id}</td>
            <td> 
            </td>
        </tr>
        <tr>
            <td>Pour mettre à jour une music </td>
            <td>Nest.js</td>
            <td>PUT</td>
            <td>api/musica/music/{id}</td>
            <td> 
            {
                "title":"Paris",
                "dateOut":"2018-01-01",
                "singer":[
                    "Niska"
                ],
                "albumId":"f3a33874-b47e-436a-8b6b-49d66ab8c381"
            }
            </td>
        </tr>
        <tr>
            <td>Pour creer un album </td>
            <td>Nest.js</td>
            <td>POST</td>
            <td>api/musica/album</td>
            <td> 
            {
                "titre":"Liens du 100",
                "dateDeSortie":"2023-01-01",
                "artistes":[
                    "SDM"
                ],
                "descritption": "LE DERNIER ALBUM DE SDM"
            }
            </td>
        </tr>
        <tr>
            <td>Prendre la liste des albums </td>
            <td>Nest.js</td>
            <td>GET</td>
            <td>api/musica/album</td>
            <td> 
            </td>
        </tr>
        <tr>
            <td>Prendre une album </td>
            <td>Nest.js</td>
            <td>GET</td>
            <td>api/musica/album/{id}</td>
            <td> 
            </td>
        </tr>
        <tr>
            <td>Pour mettre à jour un album</td>
            <td>Nest.js</td>
            <td>PUT</td>
            <td>api/musica/album/{id}</td>
            <td> 
            {
                "titre":"Liens du 100",
                "dateDeSortie":"2023-01-01",
                "artistes":[
                    "SDM"
                ],
                "descritption": "LE DERNIER ALBUM DE SDM"
            }
            </td>
        </tr>
        <tr>
            <td>Pour creer une playlist </td>
            <td>Spring Boot</td>
            <td>POST</td>
            <td>api/musica/playlist</td>
            <td> 
            {
                "name":"MADA261"
            }
            </td>
        </tr>
        <tr>
            <td>Pour prendre la liste des playlist </td>
            <td>Spring Boot</td>
            <td>GET</td>
            <td>api/musica/playlist</td>
            <td></td>
        </tr>
        <tr>
            <td>Pour prendre une playlist </td>
            <td>Spring Boot</td>
            <td>GET</td>
            <td>api/musica/playlist/{playlist}</td>
            <td></td>
        </tr>
        <tr>
            <td>Pour mettre à jour une playlist </td>
            <td>Spring Boot</td>
            <td>PUT</td>
            <td>api/musica/playlist/{playlist}</td>
            <td> 
            {
                "name":"MADA261"
            }
            </td>
        </tr>
        <tr>
            <td>Pour supprimer une playlist </td>
            <td>Spring Boot</td>
            <td>DELETE</td>
            <td>api/musica/playlist/{playlist}</td>
            <td>
            </td>
        </tr>
        <tr>
            <td>Pour ajouter des musics dans une playlist </td>
            <td>Spring Boot</td>
            <td>PUT</td>
            <td>/api/musica/music/playlist/{playlist}</td>
            <td>
            {
                [
                    "....",
                    ...
                ]
            }
            </td>
        </tr>
        <tr>
            <td>Pour Supprimer les musics d'une playlist </td>
            <td>Spring Boot</td>
            <td>DELETE</td>
            <td>/api/musica/music/playlist/{playlist}</td>
            <td>
            {
                [
                    "....",
                    ...
                ]
            }
            </td>
        </tr>
        <tr>
            <td>Pour ajouter un favoris </td>
            <td>Spring Boot</td>
            <td>POST</td>
            <td>/api/musica/favoris/{music}</td>
            <td>
            </td>
        </tr>
        <tr>
            <td>Pour supprimer un favoris </td>
            <td>Spring Boot</td>
            <td>DELETE</td>
            <td>/api/musica/favoris/{music}</td>
            <td>
            </td>
        </tr>
        <tr>
            <td>Pour recuperer les favoris</td>
            <td>Spring Boot</td>
            <td>GET</td>
            <td>/api/musica/favoris</td>
            <td>
            </td>
        </tr>
    </tbody>
</table>
