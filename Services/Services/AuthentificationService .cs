using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using Musica.Asp.DTO;
using Musica.Asp.Services.IServices;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Musica.Asp.Services.Services
{
    public class AuthentificationService : IAuthentificationService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfigurationRoot _root;
        private readonly RoleManager<IdentityRole> _roleManager;
        public AuthentificationService
            (
                UserManager<IdentityUser> userManager,
                RoleManager<IdentityRole> roleManager,
                IConfigurationRoot root)
        {
            _userManager = userManager;
            _root = root;
            _roleManager = roleManager;
        }
        public void InsertRoles(List<string> liste)
        {
            try
            {
                foreach (var role in liste)
                {
                    var roleExist = _roleManager.FindByNameAsync(role).Result;
                    if (roleExist == null)
                    {
                        var roleInserted = _roleManager.CreateAsync(new IdentityRole() { Name = role }).Result;
                        if (roleInserted.Succeeded)
                        {
                            roleExist = _roleManager.FindByNameAsync(role).Result;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Create(AuthDto userModel)
        {
            var email = userModel.Email;
            var user = new IdentityUser(email);
            user.Email = email;
            try
            {

                var success = _userManager.CreateAsync(user, userModel.Password).Result;
                if (!success.Succeeded)
                {
                    throw new InvalidOperationException(user.ToString());
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Dictionary<string, string> Login(AuthDto userModel)
        {
            try
            {
                var response = new Dictionary<string, string>();
                var user = _userManager.FindByEmailAsync(userModel.Email).Result;
                if (user != null)
                {
                    var verif = _userManager.CheckPasswordAsync(user, userModel.Password).Result;
#if DEBUG
                    response.Add("LIFE_TIME_HOURS", _root["Jwt:life"]);
#else
                    response.Add("LIFE_TIME_HOURS", Environment.GetEnvironmentVariable("JWT_LIFE"));
#endif

                    if (verif)
                    {
                        var token = GenerateJwtToken(user);
                        response.Add("JWT", token);
                        return response;
                    }
                    throw new Exception("A ERROR ON USERNAME or PASSWORD");
                }
                throw new Exception("USER NOT FOUND");
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region JWT : generate jwt 
        private string GenerateJwtToken(IdentityUser user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
#if DEBUG
            var key = Encoding.UTF8.GetBytes(_root["Jwt:Key"]);
            var life_time = int.Parse(_root["Jwt:life"]);
#else
            var key = Encoding.UTF8.GetBytes(Environment.GetEnvironmentVariable("JWT_KEY"));
            var life_time = int.Parse(Environment.GetEnvironmentVariable("JWT_LIFE"));
#endif
            var claims = new Dictionary<string, object>();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                new Claim("Id", user.Id),
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            }),
                Claims = claims,
                Expires = DateTime.UtcNow.AddHours(life_time),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };
            var token = jwtTokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = jwtTokenHandler.WriteToken(token);
            return jwtToken;
        }
        #endregion


    }
}