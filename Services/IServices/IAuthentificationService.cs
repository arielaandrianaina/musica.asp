using Musica.Asp.DTO;

namespace Musica.Asp.Services.IServices
{
    public interface IAuthentificationService
    {
        public void InsertRoles(List<string> liste);
        public void Create(AuthDto userModel);
        public Dictionary<string, string> Login(AuthDto userModel);
    }
}