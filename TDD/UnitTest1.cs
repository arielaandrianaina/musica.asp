using Microsoft.AspNetCore.Mvc;
using Moq;
using Musica.Asp.Controllers;
using Musica.Asp.DTO;
using Musica.Asp.Services.IServices;

namespace TDD
{
    public class UnitTest1
    {
        private static string JWT_MOCK = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
        private static string LIFE_TIME_HOURS_MOCK = "4";
        [Fact]
        public void ShouldLogin()
        {
            //ARRANGE 
            var serviceMock = new Mock<IAuthentificationService>();
            var httpMock = new Mock<IHttpClientFactory>();
            var loginResponse = new Dictionary<string, string>();
            loginResponse.Add("LIFE_TIME_HOURS", LIFE_TIME_HOURS_MOCK);
            loginResponse.Add("JWT", JWT_MOCK);
            serviceMock.Setup(item => item.Login(It.IsAny<AuthDto>())).Returns(loginResponse);

            //ACT 
            var controller = new AuthentificationController(serviceMock.Object, httpMock.Object);
            var result = controller.Login(It.IsAny<AuthDto>());

            //ASSERT 
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);
            var dico = (result as OkObjectResult).Value as Dictionary<string, string>;
            Assert.True(dico.ContainsKey("JWT"));
            Assert.True(dico.ContainsKey("LIFE_TIME_HOURS"));
        }

        [Fact]
        public void ShouldRegistre()
        {
            //ARRANGE 
            var serviceMock = new Mock<IAuthentificationService>();
            serviceMock.Setup(item => item.Create(It.IsAny<AuthDto>()));
            var httpMock = new Mock<IHttpClientFactory>();
            httpMock.Setup(item => item.CreateClient()).Returns(It.IsAny<HttpClient>);
            //ACT 
            var controller = new AuthentificationController(serviceMock.Object, httpMock.Object);
            var result = controller.Registre(It.IsAny<AuthDto>());

            //ASSERT  
            Assert.NotNull(result);
            Assert.IsType<OkResult>(result);
        }
    }
}