using Microsoft.AspNetCore.Mvc;
using Musica.Asp.DTO;
using Musica.Asp.Services.IServices;

namespace Musica.Asp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthentificationController : ControllerBase
    {
        private readonly IAuthentificationService _service;
        private readonly IHttpClientFactory _httpClientFactory;

        public AuthentificationController(IAuthentificationService service, IHttpClientFactory httpClientFactory)
        {
            _service = service;
            _httpClientFactory = httpClientFactory;
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] AuthDto authDto)
        {
            IActionResult response = this.BadRequest();
            try
            {
                var result = _service.Login(authDto);
                response = this.Ok(result);
            }
            catch (System.Exception)
            {
                response = this.Problem("Une erreur lors de la connexion");
            }
            return response;
        }

        [HttpPost]
        [Route("registre")]
        public IActionResult Registre([FromBody] AuthDto authDto)
        {
            IActionResult response = this.BadRequest();
            try
            {
                _service.Create(authDto);
                var httpClient = _httpClientFactory.CreateClient();
#if DEBUG
                var url = "http://localhost:8088/register?email=" + authDto.Email;
#else
                var url= "http://"+Environment.GetEnvironmentVariable("MUSICA_LUMEN_SERVICE_URL")+"/register?email="+authDto.Email;
#endif
                var httpresponse = httpClient.GetAsync(url).Result;
                if ((int)httpresponse.StatusCode == 204)
                {
                    response = this.Ok();
                }
                response = this.Ok();
            }
            catch (System.Exception)
            {
                response = this.Problem("Une erreur lors de la connexion");
            }
            return response;
        }
    }
}