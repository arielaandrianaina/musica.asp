FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 5115

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Musica.Asp.csproj", "./"]
RUN dotnet restore "Musica.Asp.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "Musica.Asp.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Musica.Asp.csproj" -c Release -o /app/publish 

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENV ASPNETCORE_URLS=http://+:5115
ENTRYPOINT ["dotnet", "Musica.Asp.dll"]
