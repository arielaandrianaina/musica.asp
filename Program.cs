#if !DEBUG
using Microsoft.EntityFrameworkCore;
using Musica.Asp.Data;
# endif
using Musica.Asp.ExtensionMethods;
using Ocelot.Middleware;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle

builder.Services.AddSingleton<IConfigurationRoot>(builder.Configuration);
// for security cors and authentification
builder.Services.SecurityInjection(builder.Configuration);
// for service and context database
builder.Services
    .ServiceInjection()
    .DbContextInjection(builder.Configuration)
    .UseOcelotConfiguration(builder.Configuration)
    .AddHttpClient();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseAuthorization();
app.UseAuthentication();
//app.MapControllers();
app.UseEndpoints(e =>
{
    e.MapControllers();
});

app.UseCors(SecurityMethods.POLICY_DEFAULT);

#if DEBUG
Console.WriteLine("NOT TO DO MIGRATION");
#else
using var scope=app.Services.CreateScope();
var services = scope.ServiceProvider;
var context = services.GetRequiredService<UserContext>();
if (context.Database.GetPendingMigrations().Any())
{
    context.Database.Migrate();
}
#endif

app.UseOcelot().Wait();
app.Run();
