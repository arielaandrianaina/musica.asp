using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Musica.Asp.Data;
using Musica.Asp.Services.IServices;
using Musica.Asp.Services.Services;

namespace Musica.Asp.ExtensionMethods
{
    public static class ServiceMethods
    {
        public static IServiceCollection ServiceInjection(this IServiceCollection services)
        {
            services.AddScoped<IAuthentificationService, AuthentificationService>();
            return services;
        }
        public static IServiceCollection DbContextInjection(this IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddDefaultIdentity<IdentityUser>(options =>
            {
                // options.SignIn.RequireConfirmedEmail = true;
            })
            .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<UserContext>();

            services.AddDbContext<UserContext>(options =>
            {
                // PAS D'extension de methode
#if DEBUG
                options.UseMySQL(configuration.GetConnectionString("UserMySQL"), mysqlOptions =>
                {

                });
#else
                options.UseMySQL(Environment.GetEnvironmentVariable("CONNECTION_MYSQL"), mysqlOptions =>
                {

                });
#endif
            });
            return services;
        }
    }

}

