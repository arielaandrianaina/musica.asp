using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace Musica.Asp.ExtensionMethods
{
    public static class SecurityMethods
    {
        public const string POLICY_DEFAULT = "POLICY_DEFAULT";
        public static void SecurityInjection(this IServiceCollection services, ConfigurationManager configuration)
        {
            CorsInjection(services, configuration);
            AuthentificationInjection(services, configuration);
        }
        public static void CorsInjection(this IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(POLICY_DEFAULT, item =>
                    item.AllowAnyHeader()
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                );
            });
        }

        public static void AuthentificationInjection(this IServiceCollection services, ConfigurationManager configuration)
        {
#if DEBUG
            var authenticationProviderKey = configuration["Jwt:ProviderKey"];
#else
            var authenticationProviderKey = Environment.GetEnvironmentVariable("PROVIDER_KEY");
#endif
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(authenticationProviderKey, options =>
            {
                string maClef = string.Empty;
#if DEBUG
                maClef = configuration["Jwt:Key"];
#else
                maClef =  Environment.GetEnvironmentVariable("JWT_KEY");
#endif
                options.SaveToken = true;
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                {
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(maClef)),
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateActor = false,
                    ValidateLifetime = true
                };
            }); ;
        }
    }
}

