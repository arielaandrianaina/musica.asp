﻿using Microsoft.IdentityModel.Tokens;
using Ocelot.Configuration.File;
using Ocelot.DependencyInjection;

namespace Musica.Asp.ExtensionMethods
{
    public static class OcelotService
    {
        public static IServiceCollection UseOcelotConfiguration(this IServiceCollection services, ConfigurationManager configuration)
        {
#if DEBUG
            configuration
                .AddJsonFile("ocelot.json", false, true);
#else
            configuration
                .AddJsonFile("ocelot.production.json", false, true);
#endif
            services.AddOcelot();
            var envs = Environment.GetEnvironmentVariables();
            services.PostConfigure<FileConfiguration>(fileConfiguration =>
            {
                if (fileConfiguration.Routes.Any())
                {
                    foreach (var route in fileConfiguration.Routes)
                    {
                        if (!route.AuthenticationOptions.AuthenticationProviderKey.IsNullOrEmpty())
                        {
                            var AUTH_PROVIDER_KEY = route.AuthenticationOptions.AuthenticationProviderKey;
                            if (AUTH_PROVIDER_KEY.StartsWith("{") && AUTH_PROVIDER_KEY.EndsWith("}"))
                            {
                                var placeHolder = AUTH_PROVIDER_KEY.TrimStart('{').TrimEnd('}');
                                var value = envs[placeHolder];
                                route.AuthenticationOptions.AuthenticationProviderKey = (string)value;
                            }
                        }
                        var HostsAndPorts = route.DownstreamHostAndPorts;
                        foreach (var HostAndPort in HostsAndPorts)
                        {
                            if (HostAndPort != null)
                            {
                                var HOST = HostAndPort.Host;
                                if (HOST.StartsWith("{") && HOST.EndsWith("}"))
                                {
                                    var placeHolder = HOST.TrimStart('{').TrimEnd('}');
                                    var value = envs[placeHolder];
                                    HostAndPort.Host = (string)value;
                                    var valuePort = Convert.ToInt32(envs[placeHolder.Replace("HOST", "PORT")]);
                                    HostAndPort.Port = valuePort;
                                }
                            }
                        }
                    }
                }
            });
            return services;
        }
    }
}
